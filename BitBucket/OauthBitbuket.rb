require 'rubygems'
require 'oauth'
require 'json'

class BitBucket 
 
  def authenticate(urlPath) 
    #My Consumer Key
    consumer_key = OAuth::Consumer.new("zUDAQRFj7q8JycDQt9","hQhEfCvmfhenBGR8SSj2kQnc4cbTaBCg")
    
    baseURL = "https://bitbucket.org/api/1.0/"
    
    address = URI("#{baseURL}#{urlPath}")
    # Set up Net::HTTP to use SSL, which is required by Twitter.
    http = Net::HTTP.new address.host, address.port
    
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    # Build the request and authorize it with OAuth.
    request = Net::HTTP::Get.new address.request_uri
    request.oauth! http, consumer_key
    
    # Issue the request and return the response.
    http.start
    response = http.request request
  end
  
  def trepScore (repoCount)
    puts "Your Own Repositories : #{repoCount}"
  end

  userName = ""
  repoCount = 0
  closeIssueCount = 0
  addedLine = 0
  removedLine = 0
  bitBucket =  BitBucket.new 
  userURL = "user/"
  response = bitBucket.authenticate(userURL)
  
  user = JSON.parse(response.body)
  
  userName = user["user"]["username"] 
  puts "Welcome #{userName} !!!!!" 
  
  # User Repositories
  
  #user["repositories"].each do |repoList|
  
      dashboardURL = "user/repositories/"
      response = bitBucket.authenticate(dashboardURL)
      dashboard = JSON.parse(response.body)
      dashboard.each do |repoList|
        
      userName = repoList["owner"]
      repo = repoList["slug"]
      puts repo
      # File Count URL 
      fileURL = "repositories/#{userName}/#{repo}/directory/"
      response = bitBucket.authenticate(fileURL)
      file = JSON.parse(response.body)
      fileCount = file["size:"]
      puts "Total No of files in #{repo} : #{fileCount}"  
      
      # Total commit
      commitURL = "repositories/#{userName}/#{repo}/changesets/?limit=50"
      response = bitBucket.authenticate(commitURL)
      commit = JSON.parse(response.body)
      commitCount = commit["count"]
      puts "Total Commit in #{repo} : #{commitCount}"  
      commit["changesets"].each do |changeset|
      rawNode = changeset["node"]
      lineCount = "repositories/#{userName}/#{repo}/changesets/#{rawNode}/diffstat/"
        response = bitBucket.authenticate(lineCount)
        line = JSON.parse(response.body)
        line.each do |lineCount|
        if lineCount["diffstat"]["added"].nil? || lineCount["diffstat"]["removed"].nil?
          lineCount["diffstat"]["added"] = 0
          lineCount["diffstat"]["removed"] = 0
        end    
          addedLine += lineCount["diffstat"]["added"]
          removedLine += lineCount["diffstat"]["removed"]
        end  
      end
      
      issueURL = "repositories/#{userName}/#{repo}/issues"
      response = bitBucket.authenticate(issueURL)
      if response.code == '200' 
        issue = JSON.parse(response.body)
              issueCount = issue["count"]
              issue["issues"].each do |issueStatus|
                if issueStatus["status"] == 'closed'
                  closeIssueCount += 1
                end
                      
              end
        
      end
      
      puts "Total Lines : #{addedLine}"
      puts "Removed Lines : #{removedLine}"
      puts "Total Issues  : #{issueCount}"
      puts "Closed Issues : #{closeIssueCount}"
      
      repoCount += 1  
  end
     
  bitBucket.trepScore(repoCount)
    
  end
  
  


